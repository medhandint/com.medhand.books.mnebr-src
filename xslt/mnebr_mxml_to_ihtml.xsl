﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" xmlns:set="http://exslt.org/sets" exclude-result-prefixes="exsl set">            

<!--
	/*************************************************************************
	 *
	 * Copyright (c) 2005+ MedHand International AB
	 * All rights reservered.
	 *
	 *************************************************************************/
	/*
	 * @(#)cdtp21_mxml_to_ihtml.xsl
   *
   * DESCRIPTION:
   * ====================
   * Stylesheet for converting MedHand XML(MXML) to iHtml.
   *
   * OVERRIDES:
   * ====================
   * img: title & caption formating 
   *
	 * CHANGES:
   * ====================
 	 * 2011-01-12 [img]: Fixed header bug for large images
	 */-->

<xsl:import href="../../common/ihtml/xslt/mxml_to_ihtml.xsl"/>
<xsl:variable name="style" select="'normal-style'"/>
<xsl:variable name="folder-max-size" select="100"/>
<xsl:param name="table-max-size" select="3"/>
<xsl:variable name="link-video-repo" select="''"/>

<!-- *******************************************************************
			BOOK SPECIFIC FORMATING
     ******************************************************************* -->

<!-- *******************************************************************
			Breadcrumbs
     ******************************************************************* -->
    

	<xsl:template name="getH2TitleColor">
	<xsl:attribute name="class">
		<xsl:text>h1-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>
	
	<xsl:template name="getSearchClassBreadcrumb">
		<xsl:choose>
			<xsl:when test="ancestor::container[contains(@p-id,'index')]"/>
			<xsl:when test="parent::container[parent::book]"/>
			<xsl:when test="contains(@class, 'chapter-foreground-color') or not(ancestor::container[contains(child::title/@class,'chapter-foreground-color')])">
				<xsl:choose>
					<xsl:when test="ancestor::container[1]/parent::book"/>
					<xsl:when test="ancestor::container[2]/parent::book">
						<xsl:value-of select="ancestor::container[2]/title"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="ancestor::container[parent::book]/title"/> - <xsl:value-of select="ancestor::container[2]/title"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="contains(ancestor::container[2]/title/@class, 'h1-foreground-color')">
						<xsl:value-of select="ancestor::container[contains(child::title/@class, 'part-foreground-color')]/title"/> - <xsl:value-of select="ancestor::container[contains(child::title/@class , 'chapter-foreground-color')]/title"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="ancestor::container[contains(child::title/@class, 'chapter-foreground-color')]/title"/> - <xsl:value-of select="ancestor::container[2]/title"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
<xsl:template name="getSearchKeywordBreadcrumb">
	<xsl:choose>	
    <xsl:when test="ancestor::container[2][not(child::title[@class = 'h1-foreground-color'])]"> 
	    <xsl:value-of select="normalize-space(ancestor::container[2][not(child::title[@class = 'h1-foreground-color'])]/title)"/> 			    
		  <xsl:text> - </xsl:text>
    </xsl:when>
  </xsl:choose>
	<xsl:if test="ancestor::container[child::title[@class = 'h1-foreground-color']][1]">
		<xsl:value-of select="normalize-space(ancestor::container[child::title[@class = 'h1-foreground-color']][1]/title)"/>
	</xsl:if>    		
</xsl:template>


<xsl:template name="getSectionBreadcrumb">
	<xsl:choose>	
    <xsl:when test="ancestor::container[2][not(child::title[@class = 'h1-foreground-color'])]"> 
	    <xsl:value-of select="normalize-space(ancestor::container[2][not(child::title[@class = 'h1-foreground-color'])]/title)"/> 			    
		  <xsl:text> - </xsl:text>
    </xsl:when>
  </xsl:choose>
	<xsl:if test="ancestor::container[child::title[@class = 'h1-foreground-color']][1]">
		<xsl:value-of select="normalize-space(ancestor::container[child::title[@class = 'h1-foreground-color']][1]/title)"/>
	</xsl:if>    	    
<!--	<xsl:value-of select="normalize-space(ancestor::container[child::title[@class = 'h1-foreground-color']][1]/title)"/>  -->
</xsl:template>

<xsl:template name="getTableBreadcrumb">
</xsl:template>

<xsl:template name="getImageBreadcrumb">
</xsl:template>

<xsl:template name="getVideoBreadcrumb">
</xsl:template>


<xsl:template match="td/p[not(normalize-space()) and not(descendant::symbol)]">
	<xsl:choose>
		<xsl:when test="descendant::img">
		<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
		<p>&#160;</p>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="tr[not(descendant::text()) and (not(descendant::img))]">
	<!--xsl:choose>
		<xsl:when test="descendant::img">
		<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
		<p>&#160;</p>
		</xsl:otherwise>
	</xsl:choose-->
</xsl:template>

<xsl:template match="td[(normalize-space(.) = 'Next')]">
  <td class="right">
    <xsl:apply-templates/>
  </td>
</xsl:template>

<!--xsl:template match="text()[normalize-space(.) = 'Previous' and (ancestor::td/p/xref[contains(@ref, 'ch') and not(contains(@ref, '-'))])]">
  <xsl:text>Previous Chapter</xsl:text>
</xsl:template>

<xsl:template match="text()[normalize-space(.) = 'Next' and (ancestor::td/p/xref[contains(@ref, 'ch') and not(contains(@ref, '-'))])]">
  <xsl:text>Next Chapter</xsl:text>
</xsl:template-->



</xsl:stylesheet>
