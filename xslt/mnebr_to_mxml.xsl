<?xml version="1.0"?>
<xsl:stylesheet xmlns:emrw="http://wiley.com/interscience/emrw" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
exclude-result-prefixes="emrw xhtml" version="1.0"
>
<xsl:output method="xml" omit-xml-declaration="yes"/>


<!--
	 *
	 * Copyright (c) 2005+ MedHand International AB
	 * All rights reservered.
	 *
	 ***************************************************************
	 *
	 * @(#)cdtp21_to_mxml.xsl
   *
  * DESCRIPTION:
   * ======================
   * Stylesheet for converting Publisher XML to MedHand XML(MXML). 
   *
   * CHANGES:
   * ======================
	 -->

<!-- PRE-DEFINED FUNCTION TEMPLATES -->

<xsl:template name="getPreviousAttributes">
	<xsl:choose>
		<xsl:when test="@*">
			<xsl:for-each select="@*">
				<xsl:text> </xsl:text><xsl:value-of select="name()"/>=<xsl:text>"</xsl:text><xsl:value-of select="."/><xsl:text>"</xsl:text>
			</xsl:for-each>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text></xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="*" mode="serialise">
  <xsl:text>&lt;</xsl:text>
  <xsl:value-of select="name()" />
  <xsl:for-each select="@*">
    <xsl:text> </xsl:text>
    <xsl:value-of select="name()" />
    <xsl:text>="</xsl:text>
    <xsl:value-of select="." />
    <xsl:text>"</xsl:text>
  </xsl:for-each>
  <xsl:text>></xsl:text>
  <xsl:apply-templates select="node()" mode="serialise" />
  <xsl:text>&lt;/</xsl:text>
  <xsl:value-of select="name()" />
  <xsl:text>></xsl:text>
</xsl:template>

<xsl:template name="getH0TitleColor">
	<xsl:attribute name="class">
		<xsl:text>h0-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<xsl:template name="getH1TitleColor">
	<xsl:attribute name="class">
		<xsl:text>h1-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<xsl:template name="getH2TitleColor">
	<xsl:attribute name="class">
		<xsl:text>h2-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<xsl:template name="getH3TitleColor">
	<i><xsl:attribute name="class">
		<xsl:text>h3-foreground-color</xsl:text>
	</xsl:attribute></i>
</xsl:template>

<xsl:template name="getH4TitleColor">
	<xsl:attribute name="class">
		<xsl:text>h4-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<xsl:template name="getH5TitleColor">
	<xsl:attribute name="class">
		<xsl:text>h5-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<xsl:template name="getBoxBackgroundColor">
	<xsl:param name="book-part-id"/>
	<xsl:attribute name="class">
		<xsl:text>inline-box-background-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<xsl:template name="getOutBoxBackgroundColor">
	<xsl:param name="book-part-id"/>
	<xsl:attribute name="class">
		<xsl:text>outline-box-background-color</xsl:text>
	</xsl:attribute>
</xsl:template>

<xsl:template name="getLinkTitleColor">
	<xsl:attribute name="class">
		<xsl:text>link-foreground-color</xsl:text>
	</xsl:attribute>
</xsl:template>


<!-- PRE-DEFINED FUNCTION TEMPLATES *END*-->

<xsl:variable name="book-id"><xsl:value-of select="/component/@id"/></xsl:variable>
<xsl:template match="manifest">
	<book pres-type="hard">
<!-- PARAMETERS FOR LINEAR READING -->	 
      <lr lrTop="1" lrBottom="1"/>
<!-- PARAMETERS FOR LINEAR READING *END* -->		
		<title><xsl:value-of select="@title"/></title>
		<img type="frontpage" src="frontpage.bmp"/>
		<xsl:apply-templates/>
	</book>
</xsl:template>

<xsl:template match="manifest-file">
	<!--xsl:choose>
		<xsl:when test="@src = 'xml\fm.xml'">
			<container pres-type="hard">
				<title search-class="title">Preliminary material</title>				
  				<xsl:apply-templates select="document(@src)"/>
			</container>		
		</xsl:when>
		<xsl:otherwise>
				<xsl:apply-templates select="document(@src)"/>
		</xsl:otherwise>
	</xsl:choose-->
	  <xsl:apply-templates select="document(@src)"/>
</xsl:template>

	<xsl:template match="part">
		<container pres-type="hard" p-id="{@id}" top-node="true">
			<title search-class="title">
				<xsl:variable name="chapterNumber">
					<xsl:value-of select="number(substring-after(@id, 'part'))"/>
				</xsl:variable>
				<xsl:attribute name="class">
					<xsl:text>part</xsl:text>
					<xsl:value-of select="$chapterNumber"/>
					<xsl:text>-foreground-color</xsl:text>
				</xsl:attribute>
				<xsl:apply-templates select="@title"/>
			</title>
			<xsl:apply-templates select="document(@src)"/>	
			<xsl:apply-templates/>
		</container>
	</xsl:template>
<!--<xsl:template match="component">
	<xsl:choose>
		<xsl:when test="@id='chowdhury2200f01'">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:when test="@id='chowdhury2200b01'">
			<container pres-type="hard" p-id="{@id}">
				<title search-class="title"><xsl:value-of select=".//unitTitle"/></title>
					<xsl:apply-templates select=".//body"/>
			</container>
		</xsl:when>
		<xsl:otherwise>
			<container pres-type="hard" p-id="{@id}">
				<title>
					<xsl:choose>
						<xsl:when test="starts-with($book-partNumber,'0')"><xsl:value-of select="substring-after($book-partNumber,'0')"/></xsl:when>
						<xsl:otherwise><xsl:value-of select="$book-partNumber"/></xsl:otherwise>
					</xsl:choose>
					<xsl:text>. </xsl:text><xsl:value-of select="./title"/>
				</title>
				<xsl:apply-templates select="./book-part"/>
			</container>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>-->

<xsl:template match="book-part-wrapper">
	<!--container pres-type="none">
		<p><i><xsl:apply-templates select="author"/></i></p>
	</container-->
    <xsl:apply-templates select="./book-part"/>
</xsl:template>



<!--first book-part 01-->


<!--xsl:template match="component[@id ='chowdhury2200f01']/titleer">
<container pres-type="hard">
<title search-class="title"><xsl:call-template name="getH2TitleColor"/>Radiology at a Glance</title>
<xsl:apply-templates select=".//creatorGroup"/>
	   <xsl:apply-templates select=".//productInfo//img"/>
	   <img src="logo.gif"/>
	   <xsl:apply-templates select=".//unitInfo"/>
</container>
</xsl:template>

<xsl:template match="component[@id ='chowdhury2200f01']/frontComponent[@id='chowdhury2200f01-fcmp-0001']">
<container pres-type="hard">
<title search-class="title"><xsl:call-template name="getH2TitleColor"/><xsl:apply-templates select="parent::component//unitTitle"/></title>
 <xsl:apply-templates/>
</container>
</xsl:template>

<xsl:template match="component[@id ='chowdhury2200f01']/frontComponent[@id='chowdhury2200f01-fcmp-0001']//verbatim">
<xsl:for-each select=".//line">
<p><i><xsl:apply-templates/></i></p>
</xsl:for-each>
</xsl:template>

<xsl:template match="component[@id ='chowdhury2200f01']/frontComponent[@id='chowdhury2200f02-fcmp-0001']">
<container pres-type="hard">
<title search-class="title"><xsl:call-template name="getH2TitleColor"/>preface and Acknowledgements</title>
<xsl:apply-templates/>
</container>
</xsl:template>

<xsl:template match="component[@id ='chowdhury2200f01']/frontComponent[@id='chowdhury2200f02-fcmp-0001']//verbatim">
<xsl:for-each select="line">
    <p>
        <xsl:apply-templates/>
    </p>
</xsl:for-each>
</xsl:template>

<xsl:template match="component[@id ='chowdhury2200f01']/frontComponent[@id='chowdhury2200f03-fcmp-0001']">
<container pres-type="hard">
<title search-class="title"><xsl:call-template name="getH2TitleColor"/>Abbreviations and Terminology</title>
<xsl:apply-templates/>
</container>
</xsl:template-->


<!--xsl:template match="abbrevList"-->
<xsl:template match="sect1[@chowdhury2200f03-sec1-0001]">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
  	<xsl:apply-templates/>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match="abbrevList">
<table border="false">
		<xsl:for-each select="abbrev">
		<tr>
		<td><xsl:apply-templates select="text()"/></td>
		<td><xsl:apply-templates select="following-sibling::termDef[1]"/></td>
		</tr>
		</xsl:for-each>
		</table>
</xsl:template>

<xsl:template match="pairedList">
<table border="false">
		<xsl:for-each select="term">
		<tr>
		<td><xsl:apply-templates select="text()"/></td>
		<td><xsl:apply-templates select="following-sibling::longDef[1]"/></td>
		</tr>
		</xsl:for-each>
		</table>
</xsl:template>

<!--xsl:template match="creatorGroup">
<xsl:for-each select="creator">
    <p>
        <b><xsl:value-of select="forenames"/><xsl:text> </xsl:text><xsl:value-of select="surname"/></b>
    </p>
	<p>
	    <xsl:value-of select="degrees"/>
	</p>
	<p>
	    <xsl:value-of select="jobTitle"/>
	</p>
	<p>
	    <xsl:apply-templates select="orgName"/><xsl:text>, </xsl:text>
	    <xsl:apply-templates select="country"/>
	</p>
</xsl:for-each>
</xsl:template>
<xsl:template match="unitInfo">
<xsl:apply-templates select=".//copyrightLine"/>
</xsl:template-->
<xsl:template match="copyright">
<container class="outline-box-background-color" pres-type="none">
<xsl:apply-templates/>
</container>
</xsl:template>

<xsl:template match="frontComponent//unitTitle">
    <title search-class="title">
	    <xsl:call-template name="getH2TitleColor"/>
	    <xsl:apply-templates/>
    </title>
</xsl:template>

<xsl:template match="ref-list">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<container class="outline-bib-background-color" pres-type="none">
	  <xsl:apply-templates/>
	</container>     
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="sidebar">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<!-- container class="inline-box-background-color" pres-type="none" -->
	<container pres-type="none">
	   <xsl:choose>
		    <xsl:when test="./title">
	             <title search-class="title">
	                <xsl:call-template name="getH2TitleColor"/>
	                    <symbol type="image" src="ess"/><xsl:text> </xsl:text>
		                 <xsl:value-of select="./title"/>
				 </title>
				  <xsl:apply-templates select="child::*[not(name() = 'title')]"/>
			</xsl:when>
			<xsl:when test="./update">
			    <title search-class="title">
	                <xsl:call-template name="getH2TitleColor"/>
	                    <symbol type="image" src="ess"/><xsl:text> </xsl:text>
			            <xsl:value-of select="./update/title"/>
				</title>
				<xsl:apply-templates/>
			</xsl:when>
		</xsl:choose>
	</container>     
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>


<xsl:template match="boxed-text">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<!-- container class="inline-box-background-color" pres-type="none" -->
	<container pres-type="none" p-id="{@id}">
	   <!--xsl:choose>
		    <xsl:when test="./title">
	             <title search-class="title">
	                <xsl:call-template name="getH2TitleColor"/>
	                    <symbol type="image" src="ess"/><xsl:text> </xsl:text>
		                 <xsl:value-of select="./title"/>
				 </title>
				  <xsl:apply-templates select="child::*[not(name() = 'title')]"/>
			</xsl:when>
			<xsl:when test="./update">
			    <title search-class="title">
	                <xsl:call-template name="getH2TitleColor"/>
	                    <symbol type="image" src="ess"/><xsl:text> </xsl:text>
			            <xsl:value-of select="./update/title"/>
				</title>
				<xsl:apply-templates/>
			</xsl:when>
		</xsl:choose-->
		<xsl:apply-templates/>
	</container>     
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="update">
   <xsl:apply-templates select="*[not(name() = 'title')]"/>
</xsl:template>

<xsl:template match="ref">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>  
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>


<xsl:template match="mixed-citation">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:choose>
		<xsl:when test="parent::p">
			<xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
			<p><xsl:apply-templates/></p> 
		</xsl:otherwise>
	</xsl:choose>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="collab">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>  
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="date-in-citation">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>  
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match="person-group">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>  
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

	<xsl:template match="name">
		<xsl:apply-templates select="given-names" />
		<xsl:text> </xsl:text>
		<xsl:apply-templates select="surname" />
	</xsl:template>

	<xsl:template match="given-names">
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="surname">
		<xsl:apply-templates />
	</xsl:template>

<xsl:template match="article-title">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>  
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="source">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>  
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="year">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match="volume">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="fpage">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="pub-id">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:apply-templates/>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>




<xsl:template match="book-part">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<container pres-type="hard" p-id="{@id}" top-node="true">
		<xsl:apply-templates/>
	</container>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="book-part-meta">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="title-group">
	<xsl:apply-templates select="title"/>
</xsl:template>

<xsl:template match="contrib-group">
<container pres-type="none">
<p><xsl:apply-templates/></p>
</container>
</xsl:template>

<xsl:template match="contrib">
<xsl:apply-templates/><xsl:if test="following-sibling::contrib"><br/></xsl:if>
</xsl:template>

<xsl:template match="contrib/name">
		<xsl:apply-templates select="given-names" />
		<xsl:text> </xsl:text>
		<xsl:apply-templates select="surname" />
</xsl:template>
<xsl:template match="contrib/name/given-names">
	<xsl:apply-templates/>
</xsl:template>
<xsl:template match="contrib/name/surname">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="body">
	<xsl:apply-templates/>
</xsl:template>


<xsl:template match="book-part/book-part-meta/title-group/title">
	<xsl:variable name="chapterNr">
	<xsl:value-of select="substring-after(parent::title-group/label, 'Chapter ')"/>
	</xsl:variable>
	<title search-class="title">

		<xsl:choose>
    <!--xsl:when test="parent::book-part/@id='app01'">
    <xsl:apply-templates/>
    </xsl:when>
    <xsl:when test="parent::book-part/@id='app02'">
    <xsl:apply-templates/>
    </xsl:when>
    <xsl:when test="parent::book-part/@id='app03'">
    <xsl:apply-templates/>
    </xsl:when>
    <xsl:when test="parent::book-part/@id='app04'">
    <xsl:apply-templates/>
    </xsl:when>
    <xsl:when test="parent::book-part/@id='app05'">
    <xsl:apply-templates/>
    </xsl:when-->

	<xsl:when test="ancestor::book-part/@id='harcarmed3'">
    <xsl:apply-templates/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="getH1TitleColor"/>	
      <xsl:value-of select="$chapterNr"/><xsl:text> </xsl:text><xsl:apply-templates/>
    </xsl:otherwise>
  </xsl:choose>
</title>

</xsl:template>

<xsl:template match="fig/title">
  <title search-class="title">
    <xsl:call-template name="getH1TitleColor"/>
    <xsl:apply-templates/>
  </title>
</xsl:template>
<!--xsl:template match="book-part/author">
<container pres-type="none"><p>
<xsl:apply-templates/>
</p></container>
</xsl:template-->

<xsl:template match="sec[@disp-level='1']">
  <xsl:variable name="str" select="./title"/>
  <xsl:choose>
    <xsl:when test="$str='&#8195;&#8195;' or normalize-space($str)=''"> 
      <xsl:apply-templates/>
    </xsl:when>
	
	<xsl:when test="(count(child::*) &lt; 2) or ((count(child::*) = 2) and child::p = '')">
      <container pres-type="none" p-id="{@id}">    
         <xsl:apply-templates/>
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
      </container>    
	    </xsl:when>
	
    <xsl:otherwise> 
      <xsl:variable name="g-id" select="generate-id(.)"/> 
      <container pres-type="hard" p-id="{@id}" top-node="true">
          <xsl:apply-templates/>
  		 <!--xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td[child::nt]"/>
  			</footnote>	
  		</xsl:if-->
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
      </container>
    </xsl:otherwise>
  </xsl:choose>         
</xsl:template>

<!--<xsl:template match="sec[@specific-use='intro']">
	<container pres-type="hard" p-id="{@id}" top-node="true">
		<xsl:apply-templates/>
	</container>
</xsl:template>-->

<xsl:template match="title[parent::sec[@specific-use='intro']]">

</xsl:template>

<xsl:template match="sec[@specific-use='sh1']">
  <xsl:variable name="str" select="./title"/>
  <xsl:choose>
    <xsl:when test="$str='&#8195;&#8195;' or normalize-space($str)=''"> 
      <xsl:apply-templates/>
    </xsl:when>
	
	<xsl:when test="(count(child::*) &lt; 2) or ((count(child::*) = 2) and child::p = '')">
      <container pres-type="none" p-id="{@id}">    
         <xsl:apply-templates/>
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
      </container>    
	    </xsl:when>
	
    <xsl:otherwise> 
      <xsl:variable name="g-id" select="generate-id(.)"/> 
      <container pres-type="hard" p-id="{@id}" top-node="true">
          <xsl:apply-templates/>
  		 <!--xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td[child::nt]"/>
  			</footnote>	
  		</xsl:if-->
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
      </container>
    </xsl:otherwise>
  </xsl:choose>         
</xsl:template>




<xsl:template match="title">
  <xsl:variable name="str" select="."/>
  <xsl:choose>
    <xsl:when test="$str='&#8195;&#8195;' or normalize-space($str)=''"> 
    </xsl:when>      
    <xsl:when test="@disp-level='1'">    
      <title search-class="title">
	      <xsl:call-template name="getH1TitleColor"/>
        <!--symbol type="image" src="bdtrif"/><xsl:text> </xsl:text--><xsl:apply-templates/>
      </title>
		</xsl:when>
		<xsl:when test="@disp-level='2'">
      <title search-class="title">
	      <xsl:call-template name="getH2TitleColor"/>
        <xsl:apply-templates/>
      </title>
		</xsl:when>
		<xsl:when test="@disp-level='3'">
		  <title search-class="title">
	      <xsl:call-template name="getH3TitleColor"/>
        <xsl:apply-templates/>
      </title>         		
		</xsl:when>
    <xsl:when test="@disp-level='4' and parent::sec/@type = 'SBH1'">    
      <title search-class="title">
	      <xsl:call-template name="getH0TitleColor"/>
        <xsl:apply-templates/>
      </title>
		</xsl:when>  		
		<xsl:when test="@disp-level='4'">
		  <xsl:choose>
		    <xsl:when test="parent::sec/parent::sec/title='REFERENCES'">	
          <title search-class="title">
    	      <xsl:call-template name="getH0TitleColor"/>
            <xsl:apply-templates/>
          </title>  
		    </xsl:when>
		    <xsl:otherwise>              			
          <title search-class="title">
    	      <xsl:call-template name="getH5TitleColor"/>
            <xsl:apply-templates/>
          </title>
        </xsl:otherwise> 
      </xsl:choose>          
		</xsl:when>
		<xsl:when test="@disp-level='5'">		
		  <xsl:choose>
		    <xsl:when test="parent::sec/@type='H2'">	
          <title search-class="title">
            <xsl:call-template name="getH5TitleColor"/>
            <symbol type="image" src="bltrif"/><xsl:text> </xsl:text><xsl:apply-templates/>
          </title>		    
		    </xsl:when>
		    <xsl:otherwise>
          <title search-class="title">
            <i><xsl:apply-templates/></i>
          </title>
        </xsl:otherwise> 
      </xsl:choose>
		</xsl:when>
		<xsl:when test="@disp-level='6'">
      <title search-class="title">
        <xsl:call-template name="getH3TitleColor"/>
        <xsl:apply-templates/>
      </title>
		</xsl:when>		
		<xsl:when test="@disp-level='7'">
      <title search-class="title">
        <xsl:call-template name="getH3TitleColor"/>
        <xsl:apply-templates/>
      </title>
		</xsl:when>				
		<xsl:when test="@disp-level='8'">
      <title search-class="title">
        <xsl:call-template name="getH3TitleColor"/>
        <xsl:apply-templates/>
      </title>
		</xsl:when>	
			<!--<xsl:when test="parent::caption/parent::table-wrap">
			<xsl:variable name="TNumber"><b><xsl:value-of select="parent::caption/preceding-sibling::label"/></b></xsl:variable>
			<title search-class="table" c-number="{$TNumber}"><b><xsl:apply-templates/></b></title>
			</xsl:when>-->
			 <xsl:when test="parent::caption/parent::table-wrap">
            
            
            
            <xsl:variable name="TNumber"><b><xsl:value-of select="parent::caption/preceding-sibling::label"/></b> </xsl:variable>
            <title><b><xsl:value-of select="$TNumber"/><xsl:text> </xsl:text><xsl:apply-templates/></b></title>  <!--NP table caption-->
            
            </xsl:when>
		<xsl:otherwise>
      <xsl:choose>
        <xsl:when test="ancestor::encyclopedia">		
		      <title search-class="title">
          	<xsl:call-template name="getH1TitleColor"/>	    
		        <xsl:apply-templates/>
		      </title>				
		    </xsl:when>
		    <xsl:otherwise>
		      <title search-class="title">		    
		        <xsl:apply-templates/>
		      </title>
		    </xsl:otherwise>
      </xsl:choose>   
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<!--xsl:template match="sect1">
<xsl:variable name="num"><xsl:value-of select="count(.//p[child::featureSimple])"/></xsl:variable>
<xsl:choose>
<xsl:when test="./noTitle">
<container pres-type="none" p-id="{@id}">
<xsl:apply-templates/>
</container>
</xsl:when>
<xsl:otherwise>
    <container pres-type="hard" p-id="{@id}">
       <xsl:apply-templates select="*[not(.//featureSimple[@type='special'])]"/>
    </container>
</xsl:otherwise>
</xsl:choose>
</xsl:template-->


<xsl:template match="sec[@disp-level='2']">
  <xsl:variable name="str" select="./title"/>
  <xsl:choose>
    <xsl:when test="$str='&#8195;&#8195;' or normalize-space($str)=''"> 
      <xsl:apply-templates/>
    </xsl:when>
	<xsl:when test="parent::sec[child::title[text()='REFERENCES'] or child::title[text()='RESOURCES FOR PRACTIONERS AND FAMILIES']]">
	    <container pres-type="none" p-id="{@id}">    
            <xsl:apply-templates/>
		</container>
	</xsl:when>
    <xsl:otherwise> 
      <xsl:variable name="g-id" select="generate-id(.)"/> 
      <container pres-type="hard" p-id="{@id}" top-node="true">    
         <xsl:apply-templates/>
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		
      </container>
    </xsl:otherwise>
  </xsl:choose>            
</xsl:template>

<xsl:template match="sec[@specific-use='sh2']">
  <xsl:variable name="str" select="./title"/>
  <xsl:choose>
    <xsl:when test="$str='&#8195;&#8195;' or normalize-space($str)=''"> 
      <xsl:apply-templates/>
    </xsl:when>
	<xsl:when test="parent::sec[child::title[text()='REFERENCES'] or child::title[text()='RESOURCES FOR PRACTIONERS AND FAMILIES']]">
	    <container pres-type="none" p-id="{@id}">    
            <xsl:apply-templates/>
		</container>
	</xsl:when>
    <xsl:otherwise> 
      <xsl:variable name="g-id" select="generate-id(.)"/> 
      <container pres-type="hard" p-id="{@id}" top-node="true">    
         <xsl:apply-templates/>
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		
      </container>
    </xsl:otherwise>
  </xsl:choose>            
</xsl:template>

<xsl:template match="sec[@disp-level='3']">
  <xsl:variable name="str" select="./title"/>
  <xsl:choose>
    <xsl:when test="$str='&#8195;&#8195;' or normalize-space($str)=''"> 
      <xsl:apply-templates/>
    </xsl:when>   
    <xsl:when test="(count(child::*) &lt; 2) or ((count(child::*) = 2) and child::p = '')">
      <container pres-type="none">    
         <xsl:apply-templates/>
  	   <!--xsl:if test="child::table-wrap//tfoot">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if-->
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
      </container>    
    </xsl:when>
    <xsl:otherwise> 
      <xsl:variable name="g-id" select="generate-id(.)"/> 
      <container pres-type="none" p-id="{@id}" top-node="true">    
         <xsl:apply-templates/>
  	   <!--xsl:if test="child::table-wrap//tfoot">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if-->
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
      </container>
    </xsl:otherwise>
  </xsl:choose>         
</xsl:template>

<xsl:template match="sec[@specific-use='sbh1']">
  <xsl:variable name="str" select="./title"/>
  <xsl:choose>
    <xsl:when test="$str='&#8195;&#8195;' or normalize-space($str)=''"> 
      <xsl:apply-templates/>
    </xsl:when>   
    <xsl:when test="(count(child::*) &lt; 2) or ((count(child::*) = 2) and child::p = '')">
      <container pres-type="hard" p-id="{@id}">    
         <xsl:apply-templates/>
  	   <!--xsl:if test="child::table-wrap//tfoot">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if-->
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
      </container>    
    </xsl:when>
    <xsl:otherwise> 
      <xsl:variable name="g-id" select="generate-id(.)"/> 
      <container pres-type="hard" p-id="{@id}" top-node="true">    
         <xsl:apply-templates/>
  	   <!--xsl:if test="child::table-wrap//tfoot">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if-->
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
      </container>
    </xsl:otherwise>
  </xsl:choose>         
</xsl:template>




<xsl:template match="sec[@disp-level='4']">
  <xsl:variable name="str" select="./title"/>
  <xsl:choose>
    <xsl:when test="$str='&#8195;&#8195;' or normalize-space($str)=''"> 
      <xsl:apply-templates/>
    </xsl:when>   
    <xsl:when test="(count(child::*) &lt; 2) or ((count(child::*) = 2) and child::p = '')">
      <container pres-type="none" p-id="{@id}">    
         <xsl:apply-templates/>
  	   <!--xsl:if test="child::table-wrap//tfoot">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if-->
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
      </container>    
    </xsl:when>
    <xsl:otherwise> 
      <xsl:variable name="g-id" select="generate-id(.)"/> 
      <container pres-type="none" p-id="{@id}" top-node="true">    
         <xsl:apply-templates/>
  	   <!--xsl:if test="child::table-wrap//tfoot">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if-->
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
      </container>
    </xsl:otherwise>
  </xsl:choose>         
</xsl:template>

<!--
<xsl:template match="sec[@title-level='4']">
  <xsl:variable name="str" select="./title"/>
  <xsl:choose>
    <xsl:when test="$str='&#8195;&#8195;' or normalize-space($str)=''"> 
      <xsl:apply-templates/>
    </xsl:when>
    <xsl:otherwise> 
      <container pres-type="none">
         <xsl:apply-templates/>
  		<xsl:if test="child::table-wrap//tfoot//td[child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
  		<xsl:if test="child::table-wrap//tfoot//td[descendant::list-item/child::nt]">
  		    <footnote p-id="{@id}">
  			    <xsl:apply-templates select="child::table-wrap//tfoot//td"/>
  			</footnote>
  		</xsl:if>
      </container>
    </xsl:otherwise>
  </xsl:choose>         
</xsl:template> -->

<xsl:template match="sec[@disp-level='5']">
  <xsl:variable name="str" select="./title"/>
  <xsl:choose>
    <xsl:when test="$str='&#8195;&#8195;' or normalize-space($str)=''"> 
      <xsl:apply-templates/>
    </xsl:when>
    <xsl:otherwise> 
      <container pres-type="none" p-id="{@id}">
         <xsl:apply-templates/>
  	  </container>
    </xsl:otherwise>
  </xsl:choose>    	  
</xsl:template>

<xsl:template match="sec[@disp-level='6']">
    <container pres-type="none" p-id="{@id}">
       <xsl:apply-templates/>
	  </container>
</xsl:template>

<xsl:template match="sec[@disp-level='7']">
    <container pres-type="none" p-id="{@id}">
       <xsl:apply-templates/>
	  </container>
</xsl:template>

<xsl:template match="sec[@disp-level='8']">
    <container pres-type="none" p-id="{@id}">
       <xsl:apply-templates/>
	  </container>
</xsl:template>

<xsl:template match="sec[@disp-level='9']">
    <container pres-type="none" p-id="{@id}">
       <xsl:apply-templates/>
	  </container>
</xsl:template>

<!--xsl:template match="title">
<xsl:choose>
<xsl:when test="parent::tabular"/>
<xsl:otherwise>
<xsl:choose>
    <xsl:when test="parent::sect1">
	 <title search-class="title">
	       <xsl:call-template name="getH2TitleColor"/>
           <xsl:apply-templates/>
        </title>
	</xsl:when>

	<xsl:otherwise>
	<title search-class="title">
        <xsl:apply-templates/>
    </title>
	</xsl:otherwise>
	</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template-->


<xsl:template match="sec">
    <container pres-type="none">
      <!--xsl:apply-templates select="./title"/>
      <xsl:apply-templates select=".//p"/-->
	  <xsl:apply-templates/>
    </container>
</xsl:template>


<xsl:template match="p">
	<xsl:choose>
	
	  <xsl:when test="@id">
	  <container p-id="{@id}" pres-type="none">
		<p><xsl:apply-templates/></p>
		<!--<xsl:apply-templates select="child::inline-graphic"/>-->
		<!--<p>
			<xsl:apply-templates select="child::*[not(self::inline-graphic)]"/>
		</p>-->
		
	</container></xsl:when>



<xsl:when test="parent::th">
	    <p><b><xsl:apply-templates/></b></p>
	  </xsl:when>
	  <xsl:when test="child::panel">
	    <xsl:apply-templates/>
	  </xsl:when>
	  

    <!--xsl:when test="(parent::list-item or parent::topic/parent::list-item) and not(preceding-sibling::p)"> 
	    <xsl:apply-templates/>
	  </xsl:when>	
    <xsl:when test="(parent::list-item or parent::topic/parent::list-item) and (preceding-sibling::p)"> 
	    <br /><xsl:apply-templates/>
	  </xsl:when-->
	  
	  <xsl:when test="parent::fn/parent::fn-group/parent::table-wrap-foot and preceding-sibling::label/child::sup">	  
		<p><sup><xsl:value-of select="preceding-sibling::label/child::sup"/></sup><xsl:text> </xsl:text><xsl:apply-templates/></p>	  
	  </xsl:when>	          	  	  
	  <xsl:otherwise>	  
      <xsl:apply-templates/>
    </xsl:otherwise>
  </xsl:choose>  
</xsl:template>
<xsl:template match="bold[1][parent::p/parent::inline-graphic]">
	<keyword search-class="image">
	<b>
	
		<xsl:apply-templates/>
	
	</b>
	</keyword>
</xsl:template>
<xsl:template match="bl">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<list>
  	<xsl:attribute name="type">
    	<xsl:choose>
    	  <xsl:when test="@type='bulleted'">
    	    <xsl:text>bullet</xsl:text>
    	  </xsl:when>
    	  <xsl:when test="@type='dash'">
    	    <xsl:text>dash</xsl:text>
    	  </xsl:when>
    	  <xsl:when test="@type='1'">
    	    <xsl:text>number</xsl:text>
    	  </xsl:when>
    	  <xsl:when test="parent::topic/parent::list-item/parent::bl and (ancestor::sidebar)">
    	    <xsl:text>bullet</xsl:text>
    	  </xsl:when>   
    	  <xsl:when test="parent::topic/parent::list-item/parent::bl">
    	    <xsl:text>plain</xsl:text>
    	  </xsl:when> 
        <xsl:when test="(parent::sidebar/preceding-sibling::title)">  
    	    <xsl:text>plain</xsl:text>
    	  </xsl:when>          	         	
    	  <xsl:otherwise>
    	    <xsl:text>bullet</xsl:text>
    	  </xsl:otherwise>
      </xsl:choose>
	  </xsl:attribute>	
		<xsl:apply-templates select="list-item"/>
	</list>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>


<xsl:template match="ul">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<xsl:choose>
	<xsl:when test="@type='plain'">
	<list type="plain">
		<xsl:apply-templates select="list-item"/>
	</list>
	</xsl:when>
	<xsl:otherwise>
	<list type="plain">
		<xsl:apply-templates select="list-item"/>
	</list>
	</xsl:otherwise>
	</xsl:choose>
	<xsl:if test="ancestor::tfoot">
	<xsl:apply-templates/>
	</xsl:if>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="list-item">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<item>
	    <!--xsl:choose>	    
		    <xsl:when test="(parent::bl/parent::topic) and not (ancestor::ul/ancestor::td) and not(ancestor::bl/ancestor::td) and not(ancestor::bl/parent::topic/ancestor::bl/parent::topic) and not(ancestor::sidebar)">
		      <p><xsl:text>&#10003; </xsl:text><xsl:apply-templates/></p>
		    </xsl:when>
		    <xsl:when test="(ancestor::bl/parent::topic/ancestor::bl/parent::topic) and not(ancestor::sidebar)">
		      <p><xsl:text>&#8226; </xsl:text><xsl:apply-templates/></p>
		    </xsl:when>
		    <xsl:when test="(parent::bl/parent::sidebar/preceding-sibling::title) and not(child::topic)"> 
          <p> 
		        <symbol type="image" src="ltrif"/><xsl:text> </xsl:text>       
            <xsl:apply-templates/>
            
          </p>  			    		    
		    </xsl:when>
		    <xsl:when test="(parent::bl/parent::sidebar/preceding-sibling::title) and (child::topic)"> 
          <p> 
		        <symbol type="image" src="ltrif"/><xsl:text> </xsl:text>
            <xsl:apply-templates/>           
          </p>
          <xsl:apply-templates select=".//fig" mode="list-item"/>  			    		    
		    </xsl:when>		    
		    <xsl:when test="ancestor::sidebar and not(child::topic)">
          <p>      
            <xsl:apply-templates/>
          </p>  			    
		    </xsl:when>		    

  			<xsl:otherwise>
  		    <p><xsl:apply-templates/></p>
  		    <xsl:apply-templates select=".//fig" mode="list-item"/>
  			</xsl:otherwise>
			</xsl:choose-->
			<xsl:apply-templates/>
		</item>
<!--	  <xsl:if test="ancestor::tfoot">
	    <p><xsl:apply-templates/></p>  
	  </xsl:if>  -->
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>


<xsl:template match="em[@activity='boldrom']">
  <xsl:choose>
  <xsl:when test="parent::p/parent::editor">
     <b><em>
      		<xsl:attribute name="class">
      				<xsl:text>text-foreground-color</xsl:text>
      		</xsl:attribute>        
          <xsl:apply-templates/>
          </em></b>
	</xsl:when>
	<xsl:otherwise>
  <b><xsl:apply-templates/></b>
  </xsl:otherwise>
  </xsl:choose>
</xsl:template>
<xsl:template match="bold">
  <xsl:choose>
  <xsl:when test="parent::p/parent::editor">
     <b><em>
      		<xsl:attribute name="class">
      				<xsl:text>text-foreground-color</xsl:text>
      		</xsl:attribute>        
          <xsl:apply-templates/>
          </em></b>
	</xsl:when>
	<xsl:when test="parent::p/parent::sec[@id='hayped24_ch10lev1sec13']">
     <b><em>
      		<xsl:attribute name="class">
      				<xsl:text>text-foreground-color</xsl:text>
      		</xsl:attribute>        
          <xsl:apply-templates/>
          </em></b>
	</xsl:when>
	<xsl:otherwise>
  <b><xsl:apply-templates/></b>
  </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="em[@activity='italic']">
   <xsl:choose>
        <xsl:when test="child::eq[@position = 'display']">
	       <xsl:apply-templates/>
		</xsl:when>
		<xsl:otherwise>
            <i><xsl:apply-templates/></i>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="italic">
   <xsl:choose>
        <xsl:when test="child::eq[@position = 'display']">
	       <xsl:apply-templates/>
		</xsl:when>
		<xsl:when test="parent::td and not(following-sibling::p)">
			<i><xsl:apply-templates/></i>
		</xsl:when>
		<xsl:when test="parent::td">
			<p><i><xsl:apply-templates/></i></p>
		</xsl:when>
		<xsl:otherwise>
            <i><xsl:apply-templates/></i>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="list-item1/title">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<p><b>
		   <xsl:apply-templates />
		</b></p>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<!--xsl:template match="topic/nl">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<list>
	<xsl:attribute name="type">
	<xsl:choose>
	<xsl:when test="@number-scheme='1'">
	    <xsl:text>number</xsl:text>
	</xsl:when>
	<xsl:when test="@number-scheme='A'">
	    <xsl:text>ALFA</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	<xsl:text>plain</xsl:text>
	</xsl:otherwise></xsl:choose>
	</xsl:attribute>
	
		<xsl:apply-templates select="level"/>
	</list>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template-->

<xsl:template match="topic">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	  <xsl:apply-templates/>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="nl">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<list>
	<xsl:attribute name="type">
	<xsl:choose>
	<xsl:when test="@number-scheme='1'">
	    <xsl:text>number</xsl:text>
	</xsl:when>
	<xsl:when test="@number-scheme='(1)'">
	    <xsl:text>number</xsl:text>
	</xsl:when>	
	<xsl:when test="@number-scheme='A'">
	    <xsl:text>ALFA</xsl:text>
	</xsl:when>
	<xsl:when test="@number-scheme='a'">
	    <xsl:text>alfa</xsl:text>
	</xsl:when>
	<xsl:when test="@number-scheme='I'">
	    <xsl:text>ROMAN</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	<xsl:text>plain</xsl:text>
	</xsl:otherwise></xsl:choose>
	</xsl:attribute>
		<xsl:apply-templates select="level"/>
	</list>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="level">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<item>
			<xsl:apply-templates/>
  		<xsl:apply-templates select=".//fig" mode="list-item"/>			
		</item>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>



<!-- USER-DEFINED ELEMENTS TEMPLATES -->  

<!-- Sections -->
<!--<xr linkends-->
<xsl:template match="xr[@linkends]">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
			
	<xsl:choose>
		<xsl:when test="contains(@linkends,'_')">
			<xsl:choose>
			  <xsl:when test="descendant::em[@activity='italic']">
			    <i><fref ref="{@linkends}" symbol="{.}">
			    </fref></i>
				</xsl:when>
				<xsl:otherwise>
			    <fref ref="{@linkends}" symbol="{.}">
			    </fref>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="parent::sec/title">
			<xref ref="{@linkends}">
			  <xsl:call-template name="getH2TitleColor"/>
			  <xsl:apply-templates/>
			</xref>
		</xsl:when>
		<xsl:otherwise>
		  <!--xsl:variable name="link" select="normalize-space(@linkends)"/>
		  <xsl:variable name="link2">
		    <xsl:choose>
			    <xsl:when test="(string-length($link) = 3) and starts-with($link, 'ch')">
		        <xsl:value-of select="concat('ch0', substring($link, 3, 1))"/- 
				  </xsl:when>
				  <xsl:otherwise>	
            <xsl:value-of select="$link"/>
				  </xsl:otherwise>
			  </xsl:choose>          	  		  
		  </xsl:variable>
			<xref ref="{$link2}">
			  <xsl:call-template name="getLinkTitleColor"/>
				<xsl:apply-templates/>
			</xref-->	 
			<xref ref="{@linkends}">
			  <xsl:call-template name="getH2TitleColor"/>
			  <xsl:apply-templates/>
			</xref>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="i">
  <i>
	<xsl:apply-templates/>
  </i>
</xsl:template>

<xsl:template match="b">
  <b>
	<xsl:apply-templates/>
  </b>
</xsl:template>

<xsl:template match="bi">
<b>
    <i>
	    <xsl:apply-templates/>
    </i>
</b>
</xsl:template>

<xsl:template match="sub">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<sub>
			<xsl:apply-templates/>
		</sub>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="sup">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
    <xsl:variable name="node-note">
      <xsl:choose>
        <xsl:when test="contains(normalize-space(.), ',')">
          <xsl:value-of select="substring-before(., ',')"/>
        </xsl:when>
        <xsl:otherwise>            
  	      <xsl:value-of select="."/>
        </xsl:otherwise>
      </xsl:choose>                            
    </xsl:variable>     	  
    <xsl:choose>
      <xsl:when test="(ancestor::table-wrap/child::table-wrap-foot//fn[descendant::sup = $node-note][1]) and not(ancestor::table-wrap-foot)">	
        <xsl:variable name="g-id" select="generate-id(ancestor::table-wrap/child::table-wrap-foot//fn[descendant::sup = $node-note][1])"/>
        <fref ref="{$g-id}" symbol="{$node-note}"/>
				<footnote p-id="{$g-id}">
			    <xsl:apply-templates select="ancestor::table-wrap/child::table-wrap-foot//fn[descendant::sup = $node-note][1]" mode="Tfootnote"/>
				</footnote>	        
        <xsl:variable name="node-note-plus">
          <xsl:choose>
            <xsl:when test="contains(normalize-space(.), ',')">
              <xsl:value-of select="substring-after(., ',')"/>
            </xsl:when>
            <xsl:otherwise>            
            </xsl:otherwise>
          </xsl:choose>                            
        </xsl:variable>  		    		    
        <xsl:if test="string-length(normalize-space($node-note-plus)) > 0">
          <xsl:call-template name="link-note">
            <xsl:with-param name="node-note-plus" select="$node-note-plus"/>
          </xsl:call-template>
        </xsl:if>                  
  		</xsl:when>
  		<xsl:when test="ancestor::fn">
  		  <sup><xsl:value-of select="."/></sup><xsl:text> </xsl:text><xsl:apply-templates select="./*"/>		
  		</xsl:when>
      <xsl:otherwise>	
    		<sup>
    			<xsl:apply-templates/>
    		</sup>
      </xsl:otherwise>
    </xsl:choose>    		   		
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template name="link-note">
  <xsl:param name="node-note-plus"/>
  <xsl:variable name="node-note">
    <xsl:choose>
      <xsl:when test="contains(normalize-space($node-note-plus), ',')">
        <xsl:value-of select="substring-before($node-note-plus, ',')"/>
      </xsl:when>
      <xsl:otherwise>            
	      <xsl:value-of select="$node-note-plus"/>
      </xsl:otherwise>
    </xsl:choose>                            
  </xsl:variable> 
  <xsl:choose>
    <xsl:when test="(ancestor::table/child::colgroup/child::tfoot//tr[descendant::sup = $node-note][1]) and not(ancestor::tfoot)">	
      <xsl:variable name="g-id" select="generate-id(ancestor::table/child::colgroup/child::tfoot//tr[descendant::sup = $node-note][1])"/>
      <xsl:text>,</xsl:text><fref ref="{$g-id}" symbol="{$node-note}"/>
			<footnote p-id="{$g-id}">
			  <p>
		    <xsl:apply-templates select="ancestor::table/child::colgroup/child::tfoot//tr[descendant::sup = $node-note][1]//td[descendant::sup = $node-note][1]" mode="note"/>
        </p> 
			</footnote>
    </xsl:when>	   
    <xsl:otherwise>
  		<sup>
  			<xsl:text>,</xsl:text><xsl:value-of select="$node-note"/>
  		</sup>    
    </xsl:otherwise>
  </xsl:choose>          
  <xsl:variable name="node-note-plus2">
    <xsl:choose>
      <xsl:when test="contains(normalize-space($node-note-plus), ',')">
        <xsl:value-of select="substring-after($node-note-plus, ',')"/>
      </xsl:when>
      <xsl:otherwise>            
      </xsl:otherwise>
    </xsl:choose>                            
  </xsl:variable>  		    		    
  <xsl:if test="string-length(normalize-space($node-note-plus2)) > 0">
    <xsl:call-template name="link-note">
      <xsl:with-param name="node-note-plus" select="$node-note-plus2"/>
    </xsl:call-template>
  </xsl:if>           
</xsl:template>


<xsl:template match="figure">
  <xsl:choose>
    <xsl:when test="(parent::td or parent::topic/parent::td or parent::topic/parent::p/parent::td) and not(child::legend)">
      <xsl:apply-templates/>
    </xsl:when>
    <xsl:when test="ancestor::list-item">    
    </xsl:when>   
    <xsl:when test="ancestor::level">    
    </xsl:when>       
    <xsl:otherwise>
      <container pres-type="none" p-id="{@id}">
        <xsl:apply-templates select="panel"/>
        <p>
          <symbol type="image" src="utrif"/><xsl:text> </xsl:text>
          <b>
      		<xsl:attribute name="class">
      				<xsl:text>text-foreground-color</xsl:text>
      		</xsl:attribute>        
          <xsl:value-of select="title"/>
          </b><xsl:text> </xsl:text><xsl:apply-templates select="legend"/>
        </p>
      </container>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="fig" mode="p">
  <container pres-type="none" p-id="{@id}">
    <xsl:apply-templates select="panel"/>
    <p>
      <symbol type="image" src="utrif"/><xsl:text> </xsl:text>
      <b><!--em>
  		<xsl:attribute name="class">
  				<xsl:text>text-foreground-color</xsl:text>
  		</xsl:attribute>        
      <xsl:value-of select="title"/>
      </em--><xsl:value-of select="title"/></b><xsl:text> </xsl:text><xsl:apply-templates select="legend"/>
    </p>
  </container>
</xsl:template>

<xsl:template match="panel">
  <xsl:choose>
    <xsl:when test="ancestor::list-item and not(parent::fig)">
      <symbol type="image" src="{@graphic}"/>
    </xsl:when>
    <xsl:otherwise>
      <img type="normal" src="{@graphic}"/>
    </xsl:otherwise>
  </xsl:choose>           
</xsl:template>

<xsl:template match="graphic">
      <img type="normal" src="{@href}"/>      
</xsl:template>
<xsl:template match="inline-graphic">
      <symbol type="image" src="{@href}"/>      
</xsl:template>






<xsl:template match="table-wrap">
	<container pres-type="none" p-id="{@id}">
    <!--xsl:if test="string-length(normalize-space(./table/title)) > 0">
      <title search-class="table"><xsl:apply-templates select="./table/title"/></title>
    </xsl:if-->    
    <xsl:apply-templates select="node()[not(self::table-wrap-foot)]"/>
	</container>
</xsl:template>	


<xsl:template match="table">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<table>
			<xsl:attribute name="border">
				<xsl:choose>
					<xsl:when test="@frame='none'">
						<xsl:text>false</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>true</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>
			<!--title search-class="table">
		<b><xsl:apply-templates select="preceding-sibling::title/text()"/></b>
	</title-->
			<xsl:apply-templates/>
			<xsl:apply-templates select="following-sibling::table-wrap-foot" mode="Tfootnote"/>
		</table>
		<!-- /container-->
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="table-wrap-foot"/>

<xsl:template match="fn-group" mode="Tfootnote">
	<xsl:apply-templates/>
</xsl:template>
<xsl:template match="fn" mode="Tfootnote">
	<xsl:apply-templates/>
</xsl:template>



<xsl:template match="table-wrap-foot" mode="Tfootnote">
		<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<xsl:variable name="colspan"><xsl:value-of select="substring-after(parent::table-wrap/descendant::colgroup/@content-type,'cols')"/></xsl:variable>
		<tr>
			<xsl:choose>
				<xsl:when test="parent::table-wrap">
					<td colspan="{$colspan}">
						<xsl:apply-templates select="descendant::fn[not(child::label/child::sup)]" mode="Tfootnote"/>
					</td>
					<!--td colspan="{$colspan}">
						<xsl:apply-templates mode="Tfootnote"/>
					</td-->
				</xsl:when>
				<xsl:otherwise>
					<td colspan="{$colspan}">
						<xsl:apply-templates mode="Tfootnote"/>
					</td>
				</xsl:otherwise>
			</xsl:choose>
		</tr>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="table/title">
  <!--xsl:if test="string-length(normalize-space(.)) > 0">
    <title search-class="table">
      <b><xsl:apply-templates/></b-->
      <xsl:apply-templates/>      
  <!--xsl:variable name="test"><xsl:value-of select="substring-after(.,' ')"/><xsl:apply-templates select="./sp"/></xsl:variable>
  <b><xsl:value-of select="substring-before(.,' ')"/></b>
  <xsl:value-of select="substring-after($test,' ')"/><xsl:apply-templates select="./sp"/-->
  
    <!--/title>
  </xsl:if-->
</xsl:template>


<!--xsl:template match="tfoot//td">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
					<p><xsl:apply-templates/></p>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template-->

<!--xsl:template match="tfoot">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<xsl:variable name="id"><xsl:value-of select=".//nt/@id"/></xsl:variable>
				<footnote p-id="{$id}">
					<p><xsl:apply-templates/></p>
				</footnote>	
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template-->

<xsl:template match="colgroup">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<xsl:apply-templates/>
		<!--xsl:apply-templates select="*[not(self::tfoot)]"/>
		<xsl:apply-templates select="./tfoot"/-->		
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="tfoot">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="thead">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<xsl:apply-templates/>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="tbody">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<xsl:apply-templates/>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>


<!--xsl:template match="td">
<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
<xsl:variable name="colspan" select="@nameend"/>  
	<td>
	<xsl:choose>
	<xsl:when test="@nameend">
	<xsl:attribute name="colspan">
		<xsl:value-of select="$colspan"/>
		  	</xsl:attribute><xsl:apply-templates/></xsl:when>
	<xsl:otherwise>
	    <xsl:apply-templates/>
		</xsl:otherwise>
		</xsl:choose>
	</td>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template-->

<xsl:template match="th">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<xsl:variable name="nameend">
			<xsl:choose>
				<xsl:when test="contains(@nameend,'col')">
					<xsl:value-of select="substring(@nameend,4)"/>
				</xsl:when>
				<xsl:when test="contains(@nameend,'c')">
					<xsl:value-of select="substring(@nameend,2)"/>
				</xsl:when>				
				<xsl:otherwise>
					<xsl:value-of select="@nameend"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="namest">
			<xsl:choose>
				<xsl:when test="contains(@namest,'col')">
					<xsl:value-of select="substring(@namest,4)"/>
				</xsl:when>
				<xsl:when test="contains(@namest,'c')">
					<xsl:value-of select="substring(@namest,2)"/>
				</xsl:when>				
				<xsl:otherwise>
					<xsl:value-of select="@namest"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<!--xsl:variable name="colspan" select="$nameend - $namest + 1"/-->
		<xsl:variable name="colspan" select="@colspan"/>
		<xsl:variable name="rowspan" select="@rowspan"/>
		
		<!---xsl:variable name="morerows" select="@morerows"/>
		<xsl:variable name="rowspan" select="$morerows + 1"/-->
		<td>
		<xsl:choose>
		<xsl:when test="child::p">
			<xsl:if test="@colspan">
				<xsl:attribute name="colspan">
					<xsl:value-of select="$colspan"/>
		  	</xsl:attribute>
		  </xsl:if>
		  <xsl:if test="@rowspan">
				<xsl:attribute name="rowspan">
					<xsl:value-of select="$rowspan"/>
		  	</xsl:attribute>
		  </xsl:if>
			<xsl:apply-templates/>
			</xsl:when>
			<xsl:otherwise>
			
			<xsl:if test="@colspan">
				<xsl:attribute name="colspan">
					<xsl:value-of select="$colspan"/>
		  	</xsl:attribute>
		  </xsl:if>
		  <xsl:if test="@rowspan">
				<xsl:attribute name="rowspan">
					<xsl:value-of select="$rowspan"/>
		  	</xsl:attribute>
		  </xsl:if>
			<p><b><xsl:apply-templates/></b></p>
			</xsl:otherwise>
			</xsl:choose>
		</td>
		
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>
<xsl:template match="td">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<xsl:variable name="nameend">
			<xsl:choose>
				<xsl:when test="contains(@nameend,'col')">
					<xsl:value-of select="substring(@nameend,4)"/>
				</xsl:when>
				<xsl:when test="contains(@nameend,'c')">
					<xsl:value-of select="substring(@nameend,2)"/>
				</xsl:when>				
				<xsl:otherwise>
					<xsl:value-of select="@nameend"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="namest">
			<xsl:choose>
				<xsl:when test="contains(@namest,'col')">
					<xsl:value-of select="substring(@namest,4)"/>
				</xsl:when>
				<xsl:when test="contains(@namest,'c')">
					<xsl:value-of select="substring(@namest,2)"/>
				</xsl:when>				
				<xsl:otherwise>
					<xsl:value-of select="@namest"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<!--xsl:variable name="colspan" select="$nameend - $namest + 1"/-->
		<xsl:variable name="colspan" select="@colspan"/>
		
		<xsl:variable name="morerows" select="@morerows"/>
		<!--xsl:variable name="rowspan" select="$morerows + 1"/-->
		<xsl:variable name="rowspan" select="@rowspan"/>
		<!--td>
			<xsl:if test="@namest">
				<xsl:attribute name="colspan">
					<xsl:value-of select="$colspan"/>
		  	</xsl:attribute>
		  </xsl:if>
		  <xsl:if test="@morerows">
				<xsl:attribute name="rowspan">
					<xsl:value-of select="$rowspan"/>
		  	</xsl:attribute>
		  </xsl:if>
			<xsl:apply-templates/>
		</td-->
		<td>
		<xsl:choose>
		<xsl:when test="child::p or child::list">
			<xsl:if test="@colspan">
				<xsl:attribute name="colspan">
					<xsl:value-of select="$colspan"/>
		  	</xsl:attribute>
		  </xsl:if>
		  <xsl:if test="@rowspan">
				<xsl:attribute name="rowspan">
					<xsl:value-of select="$rowspan"/>
		  	</xsl:attribute>
		  </xsl:if>
			<xsl:apply-templates/>
			</xsl:when>
			<xsl:otherwise>
			
			<xsl:if test="@colspan">
				<xsl:attribute name="colspan">
					<xsl:value-of select="$colspan"/>
		  	</xsl:attribute>
		  </xsl:if>
		  <xsl:if test="@rowspan">
				<xsl:attribute name="rowspan">
					<xsl:value-of select="$rowspan"/>
		  	</xsl:attribute>
		  </xsl:if>
			<p><xsl:apply-templates/></p>
			</xsl:otherwise>
			</xsl:choose>
			</td>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="td" mode="note">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="tr">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<xsl:choose>
			<xsl:when test="(ancestor::tfoot)"> 
        <xsl:variable name="node-note" select=".//td//sup[1]"/>	
        <xsl:choose>        
          <xsl:when test="(string-length(normalize-space($node-note)) > 0) and (ancestor::table/child::colgroup/*[not(node() = tfoot)]//*[contains(descendant::sup, $node-note)][1])">
          </xsl:when>		
			    <xsl:otherwise>
        		<tr>
        			<xsl:apply-templates/>
        		</tr>      
			    </xsl:otherwise>
	      </xsl:choose>					 				
			</xsl:when>		
			<xsl:otherwise>
    		<tr>
    			<xsl:apply-templates/>
    		</tr>
			</xsl:otherwise>
		</xsl:choose>	
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="title//td">
<xsl:variable name="nameend">
			<xsl:choose>
				<xsl:when test="contains(@nameend,'col')">
					<xsl:value-of select="substring(@nameend,4)"/>
				</xsl:when>
				<xsl:when test="contains(@nameend,'c')">
					<xsl:value-of select="substring(@nameend,2)"/>
				</xsl:when>				
				<xsl:otherwise>
					<xsl:value-of select="@nameend"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="namest">
			<xsl:choose>
				<xsl:when test="contains(@namest,'col')">
					<xsl:value-of select="substring(@namest,4)"/>
				</xsl:when>
				<xsl:when test="contains(@namest,'c')">
					<xsl:value-of select="substring(@namest,2)"/>
				</xsl:when>				
				<xsl:otherwise>
					<xsl:value-of select="@namest"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		
		<xsl:variable name="colspan" select="$nameend - $namest + 1"/>
		
		<xsl:variable name="morerows" select="@morerows"/>
		<xsl:variable name="rowspan" select="$morerows + 1"/>
		<td>
			<xsl:if test="@namest">
				<xsl:attribute name="colspan">
					<xsl:value-of select="$colspan"/>
		  	</xsl:attribute>
		  </xsl:if>
		  <xsl:if test="@morerows">
				<xsl:attribute name="rowspan">
					<xsl:value-of select="$rowspan"/>
		  	</xsl:attribute>
		  </xsl:if>
			<b><xsl:apply-templates/></b>
		</td>
</xsl:template>


<xsl:template match="tfoot//ul">
  <xsl:apply-templates select=".//list-item"/>
</xsl:template>

<xsl:template match="tfoot//ul/list-item">
<xsl:choose>
    <xsl:when test="child::nt">
        <footnote p-id="{nt/@id}">
			<p><xsl:apply-templates/></p>
		</footnote>
    </xsl:when>
    <xsl:otherwise>
	<xsl:for-each select=".">
        <xsl:apply-templates/><br/>
		</xsl:for-each>
    </xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="tfoot//table-wrap">
<xsl:apply-templates select=".//table"/>
</xsl:template>

<xsl:template match="tfoot//table-wrap//table">
<table border="none">
<xsl:apply-templates/>
</table>
</xsl:template>

<xsl:template match="tfoot//table-wrap//table//td">
  <td>
    <p><xsl:apply-templates/></p>
  </td>
</xsl:template>

<xsl:template match="tfoot//td[child::nt]">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
		<xsl:variable name="id"><xsl:value-of select="child::nt/@id"/></xsl:variable>
		    <footnote p-id="{$id}">
				<p><xsl:apply-templates/></p>
			</footnote>	
</xsl:template>

<xsl:template match="eq">
  <xsl:choose>
    <xsl:when test="@eq-resource = '0' and not(descendant::panel) and parent::p">
			<br /><xsl:apply-templates/><br />
    </xsl:when>
    <xsl:when test="@eq-resource = '0' and not(descendant::panel)">
			<p><xsl:apply-templates/></p>
    </xsl:when>    
    <xsl:when test="@eq-resource = '0'">
			<xsl:apply-templates/>
    </xsl:when> 
    <xsl:when test="@eq-resource = '1' and @position = 'display'">
      <xsl:choose>
        <xsl:when test="child::img"> 
          <img type="normal" src="{./img/@src}"/>
        </xsl:when> 
        <xsl:when test="parent::p"> 
		   <xsl:apply-templates/>
        </xsl:when> 
        <xsl:otherwise>
			    <p><xsl:apply-templates/></p>
        </xsl:otherwise>
      </xsl:choose>  	   			  
    </xsl:when>         
    <xsl:when test="@eq-resource = '1'">
			<img type="normal" src="{./img/@src}"/>
    </xsl:when>       
    <xsl:otherwise>
      <img type="normal" src="{@eq-resource}"/>
    </xsl:otherwise>
  </xsl:choose>  	      
</xsl:template>

<xsl:template match="br">
  <br />
</xsl:template>  

<!--xsl:template match="tfoot//td//list-item/nt">
<footnote p-id="{@id}">
				<p><xsl:apply-templates/><xsl:value-of select="parent::list-item/text()"/></p>
			</footnote>
</xsl:template-->

<xsl:template match="ext-link">
	
	
	<xsl:choose>
	<xsl:when test="@ext-link-type='rid'">
	<xref ref="{@href}">
		<xsl:call-template name="getH2TitleColor"/>
		<xsl:apply-templates/>
	</xref>
	</xsl:when>
	<xsl:otherwise>
    <xref type="www" ref="{.}">
		<xsl:apply-templates/>
	</xref>
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="encyclopedia">
  <container pres-type="hard" p-id="{@id}">
    <title>
      <xsl:call-template name="getH1TitleColor"/>
      <xsl:value-of select="main-title/title"/>
    </title>    
    <p><b><xsl:value-of select="main-title/edition"/></b><br /></p>
    <xsl:apply-templates select=".//editors"/>
    <xsl:apply-templates select=".//publisher"/>
		<p><xsl:apply-templates select=".//copyright-notice//copyright"/></p>
		<p><xsl:apply-templates select=".//copyright-notice//rights"/></p>
		<p><xsl:apply-templates select=".//copyright-notice//printed"/></p>
		<xsl:apply-templates select=".//CIP-data"/>
	</container>
	<container pres-type="hard" p-id="{@id}">
	  <title>
      <xsl:call-template name="getH1TitleColor"/>   
      <xsl:value-of select="contributors/title"/>
    </title>    	
    <xsl:for-each select=".//contributor">
<!--      <p><b><xsl:value-of select="nm"/></b></p> -->
      <xsl:apply-templates select="nm"/>
  	  <xsl:for-each select="affiliations">
  	    <p><xsl:apply-templates/><xsl:apply-templates select="parent::contributor/nt"/></p>
  	  </xsl:for-each>  	  
    </xsl:for-each>
	</container>
<!--		<container pres-type="hard">
		    <xsl:apply-templates select="Acknowledgments"/>
		</container>
		<container pres-type="hard">
		    <xsl:apply-templates select="Reader"/>
		</container>  -->
		<container pres-type="hard" p-id="{@id}">
		    <xsl:apply-templates select="preface"/>
		</container>
</xsl:template>

<xsl:template match="editors">
  <p><xsl:apply-templates select="title"/></p>
  <xsl:for-each select="editor">
<!--      <p><b><xsl:value-of select="nm"/></b></p>  -->
    <xsl:apply-templates select="nm"/>
  	<xsl:for-each select="affiliations">
  	   <p><xsl:apply-templates/></p>
  	</xsl:for-each>
  </xsl:for-each>
</xsl:template>

<xsl:template match="author">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="nm">
  <!--xsl:choose>
    <xsl:when test="ancestor::author">
			<p><xsl:apply-templates/></p>
    </xsl:when>
    <xsl:otherwise>
    	<container pres-type="none">
    	  <title>
          <xsl:call-template name="getH1TitleColor"/>
          <xsl:apply-templates/>
        </title>   		  
    	</container>
    </xsl:otherwise>
  </xsl:choose--> 
    <p> <b><em>
      		<xsl:attribute name="class">
      				<xsl:text>text-foreground-color</xsl:text>
      		</xsl:attribute>        
          <xsl:value-of select="."/>
          </em></b></p>
</xsl:template>

<!--xsl:template match="publisher">
    <img type="normal" src="mcgrewHillLogo.png"/>
    <p><xsl:value-of select="child::block[1]/line[1]"/></p>
    <p><xsl:value-of select="child::block[2]/line[1]"/></p>
    <p><xsl:value-of select="child::block[3]/line[1]"/></p>
    <p><xsl:value-of select="child::block[4]/line[1]"/></p>
    <img type="normal" src="Logo1.png"/>
</xsl:template-->

<xsl:template match="editor[parent::sec[@id='authors']]">
<container pres-type="none">
<xsl:apply-templates/>
</container>
</xsl:template>





<!--

<xsl:template match="block[@ref]">
<container pres-type="none">
<xsl:call-template name="getBoxBackgroundColor"/>
    <title search-class="title">
	    <xsl:call-template name="getH2TitleColor"/>
            <xsl:value-of select="child::line[1]"/>
    </title>
	<xsl:apply-templates select="child::line[2]"/>
</container>
</xsl:template>
<xsl:template match="block[@ref]/line[2]">
<p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="CIP-data">
    <xsl:apply-templates select="isbn"/>
		<container class="outline-box-background-color" pres-type="none">
		  <xsl:apply-templates select="ancestor::encyclopedia/metadata/process"/>
		</container>        
    <xsl:for-each select="block">
	<xsl:for-each select="line">
	   <p><xsl:apply-templates/></p>
	</xsl:for-each>
</xsl:for-each>
</xsl:template>		
-->
<xsl:template match="preface">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="preface/title">
  <title search-class="title">
    <xsl:call-template name="getH1TitleColor"/>  
    <xsl:value-of select="."/>
  </title>
</xsl:template>

<xsl:template match="Acknowledgments">
   <xsl:apply-templates/>
</xsl:template>
<xsl:template match="Acknowledgments/title">
    <title><xsl:value-of select="."/></title>
</xsl:template>
<xsl:template match="Reader">
    <xsl:apply-templates/>
</xsl:template>
<xsl:template match="Reader/title">
    <title><xsl:value-of select="."/></title>
</xsl:template>


<xsl:template match="xref">
<!--fref ref="{@ref}" symbol="{.}"/-->
	<xref ref="{@rid}">
		<xsl:call-template name="getH2TitleColor"/>
		<xsl:apply-templates/>
	</xref>
</xsl:template>


<xsl:template match="note">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	    <xsl:variable name="id"><xsl:value-of select="@id"/></xsl:variable>
			<footnote p-id="{@id}">
				<xsl:apply-templates/>
		    </footnote>	
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="label">
	<xsl:choose>
		<xsl:when test="ancestor::title-group/ancestor::book-part-meta"></xsl:when>
		<xsl:when test="parent::table-wrap"></xsl:when>
		<xsl:when test="parent::fig"></xsl:when>
		<xsl:when test="parent::fn/ancestor::table-wrap-foot"></xsl:when>
		<xsl:otherwise>
			<xsl:apply-templates/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="caption">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="list">
	<xsl:comment>&lt;<xsl:value-of select="name()"/><xsl:call-template name="getPreviousAttributes"/>&gt;</xsl:comment>
	<list>
  	<xsl:attribute name="type">
    	<xsl:choose>
    	  <xsl:when test="@list-type='bulleted'">
    	    <xsl:text>bullet</xsl:text>
    	  </xsl:when>
    	  <xsl:when test="@list-type='dash'">
    	    <xsl:text>dash</xsl:text>
    	  </xsl:when>
    	  <xsl:when test="@list-type='number'">
    	    <xsl:text>number</xsl:text>
    	  </xsl:when>
    	  <xsl:when test="parent::topic/parent::list-item/parent::bl and (ancestor::sidebar)">
    	    <xsl:text>bullet</xsl:text>
    	  </xsl:when>   
    	  <xsl:when test="parent::topic/parent::list-item/parent::bl">
    	    <xsl:text>plain</xsl:text>
    	  </xsl:when> 
        <xsl:when test="(parent::sidebar/preceding-sibling::title)">  
    	    <xsl:text>plain</xsl:text>
    	  </xsl:when>          	         	
    	  <xsl:otherwise>
    	    <xsl:text>bullet</xsl:text>
    	  </xsl:otherwise>
      </xsl:choose>
	  </xsl:attribute>	
		<xsl:apply-templates select="list-item"/>
	</list>
	<xsl:comment>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:comment>
</xsl:template>

<xsl:template match="disp-formula">
<p><xsl:apply-templates/></p>
</xsl:template>
<xsl:template match="sig-block">
<p indent="4"><xsl:apply-templates/></p>
</xsl:template>
<xsl:template match="sig">
<xsl:apply-templates/>
<xsl:if test="following-sibling::sig"><br/></xsl:if>
</xsl:template>


<xsl:template match="question-wrap">
	<container pres-type="none" p-id="{@id}"  class="inline-box-background-color">	
	<xsl:apply-templates mode="correct"/>
	
	<container p-id="{@id}-{generate-id(.)}" pres-type="soft">
		<title>Answer</title>
		
		<xsl:apply-templates select="child::answer-set/answer[@correct='yes']"/>
	
	</container>
	</container>
</xsl:template>

<xsl:template match="p[parent::question]"  mode="correct">
	<container pres-type="none" p-id="{@id}">
		<p><xsl:apply-templates/></p>
	</container>
</xsl:template> 


<xsl:template match="question">
	<p>
	
	<xsl:apply-templates select="label"/><xsl:text> </xsl:text>
	
	<xsl:apply-templates select="p"/>
	</p>
</xsl:template>



<xsl:template match="answer-set">
	<xsl:apply-templates />
</xsl:template>

<xsl:template match="answer" mode="correct">
	
	
	<xsl:apply-templates select="label"/>
	
	<xsl:apply-templates select="p" mode="correct"/>
	
</xsl:template>

<xsl:template match="p[parent::answer]" mode="correct">
	<container pres-type="none" p-id="{@id}">
		<p><xsl:apply-templates/></p>
	</container>
</xsl:template> 

<xsl:template match="answer">
	
	
	<xsl:apply-templates select="label"/>
	
	<xsl:apply-templates select="p"/>
	
</xsl:template>

	<xsl:template match="explanation" mode="correct">
			<container pres-type="soft">
					<title>Explanation</title>
					
		<xsl:apply-templates />
	</container>
	</xsl:template>

<xsl:template match="p[parent::answer or parent::question]">
<p><xsl:apply-templates/></p>
</xsl:template> 
<xsl:template match="label[parent::answer or parent::question]">
	<p><xsl:apply-templates/></p>
</xsl:template> 


<!--<xsl:template match="answer[starts-with(@id,'neuroexam3_ch')]">
<p><xsl:apply-templates select="label"/>
	<xsl:text>&#160;</xsl:text>
	<xsl:apply-templates select="p"/></p>
</xsl:template>-->

<!--INDEX-->
<!-- the index is missing the reference, so i just did not handle it-->

<!-- This is were a unndefined element ends up. -->
<xsl:template match="xhtml:*">
	[-- No stylesheet td for: <xsl:value-of select="name()"/> --]
</xsl:template>

<!-- UNDEFINED ELEMENT TEMPLATES *END*-->

</xsl:stylesheet>
